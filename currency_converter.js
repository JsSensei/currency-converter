//Create class for our custom Element

/**
 * User defined styles overwrites the style that could be potentially defined here
 * so to style this element, simply provide user defined styles on the page using the element
 */
class CurrencyConverter extends HTMLElement {
    constructor() {
        super();

        /**
         * the attribute `currency-obj` can be used to pass a currencies and values
         * It is expected be an object with a specific structure( see getDefaultCurrencyObj function)
         */
        const currencyObj = this.hasAttribute('currency-obj') ? this.getAttribute('currency-obj') : 
            getDefaultCurrencyObj();

        const shadow = this.attachShadow({ mode: 'open'});
        
        const wrapper = document.createElement('div');
        wrapper.setAttribute('class', 'currency-converter');
        
        const inputAmountText = document.createElement('span');
        inputAmountText.setAttribute('name', 'input-amount-text');

        const lineBreak = document.createElement('br');
        const outputAmountText = document.createElement('span');
        outputAmountText.setAttribute('name', 'output-amount-text');
        
        wrapper.appendChild(inputAmountText);
        wrapper.appendChild(lineBreak);
        wrapper.appendChild(outputAmountText);
        
        const inputDiv = document.createElement('div');
        inputDiv.setAttribute('class', 'cc-input');

        const inputAmountInput = document.createElement('input');
        inputAmountInput.addEventListener('input', function() {
            updateInputAmount(shadow, currencyObj);
        });
        inputAmountInput.setAttribute('type', 'number');
        inputAmountInput.setAttribute('name', 'input-amount');
        inputAmountInput.setAttribute('value', '1');

        const baseCurrencySelect = document.createElement('select');
        baseCurrencySelect.setAttribute('name', 'base-currency');
        baseCurrencySelect.addEventListener('change', function() {
            let selectedInputCurrency = this.options[ this.selectedIndex ].value;
            updateTargetCurrencySelect( selectedInputCurrency, shadow, currencyObj);
        });

        Object.keys(currencyObj.values).forEach(currencyCode => {
            let currencyOption = document.createElement('option');
            currencyOption.setAttribute('value', currencyCode);
            currencyOption.textContent = currencyObj.currencies[ currencyCode ];

            baseCurrencySelect.appendChild( currencyOption );
        });

        inputDiv.appendChild(inputAmountInput);
        inputDiv.appendChild(baseCurrencySelect);
        wrapper.appendChild(inputDiv);
    

        const outputDiv = document.createElement('div');
        outputDiv.setAttribute('class', 'cc-output');

        const outputAmountInput = document.createElement('input');
        outputAmountInput.setAttribute('name', 'output-amount');
        outputAmountInput.setAttribute('readonly', 'readonly');

        const targetCurrencySelect = document.createElement('select')
        targetCurrencySelect.setAttribute('name', 'target-currency');
        targetCurrencySelect.addEventListener('change', function() {
            let selectedTargetCurrency = this.options[ this.selectedIndex ].value;
            performConversion( selectedTargetCurrency, shadow, currencyObj);
        });

        const defaultSelectedInputCurrency = Object.keys(currencyObj.values)[ 0 ];

        setupTargetCurrencySelect(targetCurrencySelect, Object.keys(currencyObj.values[ defaultSelectedInputCurrency ]), currencyObj);

        outputDiv.appendChild(outputAmountInput);
        outputDiv.appendChild(targetCurrencySelect);
        wrapper.appendChild(outputDiv);

        shadow.appendChild(wrapper);
    }
}

//Define the new element
customElements.define('currency-converter', CurrencyConverter);

/////////////////////////////////////////////////////////////////////////////////////////////////////////

function getDefaultCurrencyObj() {
    return {
        'currencies': {
            'EUR': 'Euro',
            'USD': 'US Dollar',
            'CHF': 'Swiss Franc',
            'JPY': 'Japan Yen',
            'CAD': 'Canada Dollar',
            'GBP': 'Great Britain Pound'
        },
        'values': {
            'EUR': {
                'USD': 1.2897,
                'CHF': 1.3135,
                'GBP': 0.8631
             },
            'USD': {
                'JPY': 109.6200
             },
             'CHF': {
                 'USD': 0.9960
             },
             'GBP': {
                 'CAD': 1.7574
             }
        }
   }
}


function updateTargetCurrencySelect(selectedInputCurrency, shadow, currencyObj) {
    shadow.querySelector('[name="output-amount"]').value = '';
    let targetCurrencySelect = shadow.querySelector('[name="target-currency"]');
    targetCurrencySelect.innerHTML = '';
    resetTexts(shadow);
    setupTargetCurrencySelect(targetCurrencySelect, Object.keys(currencyObj.values[ selectedInputCurrency ]), currencyObj);
}

function setupTargetCurrencySelect(selectElem, currenciesKeys, currencyObj) {
    const disabledOption = document.createElement('option');
    disabledOption.setAttribute('disabled', 'disabled');
    disabledOption.setAttribute('selected', 'selected');
    disabledOption.setAttribute('value', 'DEFAULT');
    disabledOption.textContent = 'Select target Currency';
        
    selectElem.appendChild(disabledOption);

    currenciesKeys.forEach(currencyCode => {
        let targetCurrencyOption = document.createElement('option');
        targetCurrencyOption.setAttribute('value', currencyCode);
        targetCurrencyOption.textContent = currencyObj.currencies[ currencyCode ];
        selectElem.appendChild(targetCurrencyOption);
    })
}

function updateInputAmount(shadow, currencyObj) {
    let targetCurrencySelect = shadow.querySelector('[name="target-currency"]');
    let targetCurrency = targetCurrencySelect.options[ targetCurrencySelect.selectedIndex].value;
    if(targetCurrency !== 'DEFAULT') {
        performConversion(targetCurrency, shadow, currencyObj);
    }else{
        resetTexts(shadow);
    }

}

function resetTexts(shadow) {
    shadow.querySelector('[name="input-amount-text"]').textContent = '';
    shadow.querySelector('[name="output-amount-text"]').textContent = '';
}

function performConversion(targetCurrency, shadow, currencyObj){
    let baseCurrencySelect = shadow.querySelector('[name="base-currency"]')
    let baseCurrency = baseCurrencySelect.options[ baseCurrencySelect.selectedIndex].value;
    let conversionRate = currencyObj.values[ baseCurrency ][ targetCurrency ];

    let inputAmount = shadow.querySelector('[name="input-amount"]').value;
    let outputAmount = inputAmount * conversionRate;
    shadow.querySelector('[name="output-amount"]').value = outputAmount;
    shadow.querySelector('[name="input-amount-text"]').textContent = `${inputAmount} 
    ${currencyObj.currencies[ baseCurrency ]} equals`;
    shadow.querySelector('[name="output-amount-text"]').textContent = `${outputAmount} 
    ${currencyObj.currencies[ targetCurrency ]}`;
}